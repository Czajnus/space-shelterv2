﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BannerScript : MonoBehaviour {
    private GameObject inProgressMenu;

	// Use this for initialization
	void Start () {
        
	}

    public void OnMouseDown()
    {
        if (GameObject.Find("mainObject").GetComponent<GameConstants>().click == false)
        {
            inProgressMenu = GameObject.Find("InProgressCanvas");
            inProgressMenu.GetComponent<Canvas>().enabled = true;
            GameObject.Find("mainObject").GetComponent<GameConstants>().click = true;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
