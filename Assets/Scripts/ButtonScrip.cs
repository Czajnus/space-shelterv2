﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScrip : MonoBehaviour {

    private IAPManager cos;
    private GameObject purchaseMenu;

    private void Start()
    {
        cos = new IAPManager();
        purchaseMenu = GameObject.Find("PurchaseMenu");
        //cos = GetComponent<IAPManager>();
    }



	public void Buy50Points()
    {
        //IAPManager.Instance.Buy50Points();
        cos.Buy50Points();
        
    }
    public void Buy100Points()
    {
      //  IAPManager.Instance.Buy50Points();
        cos.Buy100Points();
    }
    public void Buy200Points()
    {
       // IAPManager.Instance.Buy50Points();
        cos.Buy200Points();
    }
    public void CLose()
    {
        GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
        purchaseMenu.GetComponent<Canvas>().enabled = false;
    }

    public void OpenMenu()
    {
        if (GameObject.Find("mainObject").GetComponent<GameConstants>().click == false)
        {
            GameObject.Find("mainObject").GetComponent<GameConstants>().click = true;
            GameObject.Find("MenuCanvas").GetComponent<Canvas>().enabled = true;
        }
    }

    public void ResumeGame()
    {
        GameObject.Find("MenuCanvas").GetComponent<Canvas>().enabled = false;
        GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
    }
    public void ExitGame()
    {
        Application.Quit();
    }

    public void OpenQuestLog()
    {
        GameObject.Find("QuestCanvas").GetComponent<Canvas>().enabled = true;
    }

    public void CloseQuestLog()
    {
        GameObject.Find("QuestCanvas").GetComponent<Canvas>().enabled = false;
    }
}
