﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildButtonScript : MonoBehaviour {


    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnMouseDown() {
        var room = GameObject.Find("mainObject").GetComponent<Main>().RoomByGameObject(gameObject);
        GameObject.Find("mainObject").GetComponent<Main>().ClearGreen();
        Main.X = room.x;
        Main.Y = room.y;
        print(Main.X + "  " + Main.Y);
        print(room.x + " a " + room.y);
        GameObject.Find("mainObject").GetComponent<Main>().AddRoom(room.x, room.y, false);


    }


}
