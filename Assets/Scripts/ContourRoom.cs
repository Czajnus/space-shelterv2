﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContourRoom : Room {
    
    public ContourRoom(GameObject go) : base(go) {

    }

    public ContourRoom(GameObject go, int x, int y) : base(go, x, y) {

    }

}
