﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeManager : MonoBehaviour {

    private Vector2 startPosition;
    private Vector2 endPosition;
    private float startTime;
    private float endTime;
    private float swipeDistance;
    private float swipeTime;

    public Sprite rightArrow;
    public Sprite leftArrow;
    public Sprite upArrow;
    public Sprite downArrow;

    public float swipeMax = 4;
    public float minSwipeDist = 1;

    private int x;
    private int y;

    private GameObject camera;

   /* private void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                startTime = Time.time;
                startPosition = touch.position;

            }

            else if (touch.phase == TouchPhase.Ended)
            {
                endTime = Time.time;
                endPosition = touch.position;

                swipeDistance = (endPosition - startPosition).magnitude;
                swipeTime = endTime - startTime;
            }

            if (swipeTime <= swipeMax && swipeDistance > minSwipeDist)
            {

                Swipe();

            }
        }
    }

    private void Swipe()
    {
        Vector2 distance = endPosition - startPosition;


        if (Mathf.Abs(distance.x) > Mathf.Abs(distance.y))
        {

            Debug.Log("Horizontal Swipe!");
            if (distance.x > 0)
            {
                camera = GameObject.Find("Main Camera");
                camera.GetComponent<Camera>().transform.position;
                GameObject.Find("Pacman").GetComponent<pacman>().flag = 4;
                GameObject.Find("Arrow").GetComponent<SpriteRenderer>().sprite = rightArrow;
            }
            if (distance.x < 0)
            {

                GameObject.Find("Pacman").GetComponent<pacman>().flag = 3;
                GameObject.Find("Arrow").GetComponent<SpriteRenderer>().sprite = leftArrow;
            }
        }

        else if (Mathf.Abs(distance.x) < Mathf.Abs(distance.y))
        {
            Debug.Log("Vertical Swipe!");
            if (distance.y > 0)
            {
                GameObject.Find("Pacman").GetComponent<pacman>().flag = 1;
                GameObject.Find("Arrow").GetComponent<SpriteRenderer>().sprite = upArrow;
            }
            if (distance.y < 0)
            {
                GameObject.Find("Pacman").GetComponent<pacman>().flag = 2;
                GameObject.Find("Arrow").GetComponent<SpriteRenderer>().sprite = downArrow;
            }
        }
    }
}*/
}
