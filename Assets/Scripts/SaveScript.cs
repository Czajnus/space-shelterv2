﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveScript : MonoBehaviour
{
    public float[] money = new float[4];
    public Dictionary<int, List<Room>> flor = new Dictionary<int, List<Room>>();
    public List<string> list1 = new List<string>();
    public string rob;
    private bool cos=false;
    private bool ready = false;
    private bool cos2 = false;

    private GameObject buildingInProgress;

    public string fjson;
    // Use this for initialization
    void Start()
    {
        Load();
        
        flor = GameObject.Find("mainObject").GetComponent<Main>().floors;

    }

    // Update is called once per frame
    void Update()
    {
        if (ready == true)
        {
            if (cos == false)
            {

                StartCoroutine(WwW());
                cos = true;

            }
        }

        if (cos2 == false)
        {
            cos2 = true;
            StartCoroutine(Ready());
            
        }
        

        if (Input.GetKeyDown(KeyCode.Q))
        {
            
             Save();
        }


    }

    public IEnumerator WwW()
    {


        yield return new WaitForSeconds(1f);
        try
        {
            Save();
        }
        catch
        {
            Debug.Log("Fail");
        }
        cos = false;

        
    }
    public IEnumerator Ready()
    {


        yield return new WaitForSeconds(3f);
        Debug.Log("READY!!");
        ready = true;


    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        if (!File.Exists(Application.dataPath + "/playerData.dat"))
        {
            FileStream file2 = File.Create(Application.dataPath + "/playerData.dat");
        }
        FileStream file = File.Open(Application.dataPath + "/playerData.dat", FileMode.Open);
        

        List<RoomInfo> list2 = new List<RoomInfo>();

        for (int i = 0; i <= 10; i++)
        {
            for (int j = 0; j <= 10; j++)
            {
                try
                {
                   // print("try" + i);
                    rob = flor[i][j].gameObject.name;
                   // print("rob"+rob);
                    RoomInfo info = new RoomInfo();
                    info.level = flor[i][j].gameObject.GetComponent<UpgradeMenuScript>().roomLvl;
                    info.type = flor[i][j].gameObject.GetComponent<UpgradeMenuScript>().roomKind;
                    info.name = flor[i][j].gameObject.name;
                    info.x = j;
                    info.y = i;
                    info.upgrading = flor[i][j].gameObject.GetComponent<UpgradeMenuScript>().upgrading;
                    list2.Add(info);
                    
                    
                    //print(flor[i][j].gameObject.name);
                }
                catch (Exception e)
                {

                }
            }
        }
        string playerToJayson = JsonHelper.ToJson(list2.ToArray(), true);
         string json = JsonUtility.ToJson(list2);
        //print("jsooon: " + list2.Count);
        File.WriteAllText(Application.dataPath + "/roomData.json", playerToJayson);


        PlayerData data = new PlayerData();
        data.money[0] = GameConstants.Money;
        data.money[1] = GameConstants.RMoney;
        data.money[2] = GameConstants.YMoney;
        data.money[3] = GameConstants.PMoney;
        data.RoomCount = GameConstants.RoomCount;
        data.RRoomCount = GameConstants.RRoomCount;
        data.YRoomCount = GameConstants.YRoomCount;
        data.PRoomCount = GameConstants.PRoomCount;
        data.MagazineCount = GameConstants.MagazineCount;
        data.buildLimit = GameConstants.buildLimit;
        data.AllRoomCount = GameConstants.AllRoomCount;
        data.mainCost = GameConstants.mainCost;
        data.building = Main.building;
        data.whiteCost1 = GameConstants.whiteCost1;
        data.redCost1 = GameConstants.redCost1;
        data.redCost2 = GameConstants.redCost2;
        data.yellowCost1 = GameConstants.yellowCost1;
        data.yellowCost2 = GameConstants.yellowCost2;
        data.purpleCost1 = GameConstants.purpleCost1;
        data.purpleCost2 = GameConstants.purpleCost2;

        try
        {
            data.bannerX = GameObject.Find("banner(Clone)").transform.position.x;
            data.bannerY = GameObject.Find("banner(Clone)").transform.position.y;
            data.bannerZ = GameObject.Find("banner(Clone)").transform.position.z;
            data.timeTextX = GameObject.Find("Text(Clone)").transform.position.x;
            data.timeTextY = GameObject.Find("Text(Clone)").transform.position.y;
            data.timeTextZ = GameObject.Find("Text(Clone)").transform.position.z;
            data.StartTime = Main.StartTime;
            data.EndTime = Main.EndTime;
            data.ConTime = Main.CorTime;
            
            if (Main.X >= 0 && Main.Y >= 0)
            { 
                data.X = Main.X;
                data.Y = Main.Y;
                data.licz = BuildMenuScript.licz;
            }
            /*print("X: " + Main.X);
            print("Y: " + Main.Y);
            print("Licz: " + BuildMenuScript.licz);*/
            
        }
        catch
        {
           // print("No Banner");
        }
        

       /* print("Baner pos: " + data.bannerX + data.bannerY + data.bannerZ);
        print("ST: " + data.StartTime);
        print("ET: " + data.EndTime);
        print("CT: " + data.ConTime);
        print("Building: " + data.building);

        print("Magazine Count: "+data.MagazineCount);*/

        bf.Serialize(file, data);
        file.Close();
       // print("Save Complete!");
    }

    public void Load()
    {
        


        // TUTAJ LOAD!!
       if (File.Exists(Application.dataPath + "/roomData.json"))
        {
            //print("ROOM INFO");
            string s = File.ReadAllText(Application.dataPath + "/roomData.json");

            RoomInfo[] room = JsonHelper.FromJson<RoomInfo>(s);

            for (int i = 1; i < room.Length; i++)
            {
               /* print(room[i].name);
                print(room[i].level);
                print(room[i].type);
                print(room[i].upgrading);
                print(room[i].x);
                print("y: "+room[i].y);
                */
                if (!GameObject.Find("mainObject").GetComponent<Main>().floors.ContainsKey(room[i].y))
                {
                    GameObject.Find("mainObject").GetComponent<Main>().AddElevator( room[i].y, false);
                }
                Main.aftcounter = true;
                Main.counter = false;
                if (room[i].type == 1)
                {
                    BuildMenuScript.licz = 0;
                }
                if (room[i].type == 2)
                {
                    BuildMenuScript.licz = 1;

                }
                if (room[i].type == 3)
                {
                    BuildMenuScript.licz = 2;

                }
                if (room[i].type == 4)
                {
                    BuildMenuScript.licz = 3;

                }
                if (room[i].type == 5)
                {
                    BuildMenuScript.licz = 4;
                }
                
                GameObject.Find("mainObject").GetComponent<Main>().AddRoom(room[i].x, room[i].y, false);
                Main.aftcounter = false;
                
                if (room[i].type == 2)
                {
                    if (room[i].level == 1)
                    {
                        //BuildMenuScript.licz = 0;
                    }
                    if (room[i].level == 2)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().WroomLv2;
                    }
                    if (room[i].level == 3)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().WroomLv3;

                    }
                    if (room[i].level == 4)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().WroomLv4;

                    }
                }
                if (room[i].type == 3)
                {
                    if (room[i].level == 1)
                    {
                        //BuildMenuScript.licz = 0;
                    }
                    if (room[i].level == 2)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().RroomLv2;
                    }
                    if (room[i].level == 3)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().RroomLv3;

                    }
                    if (room[i].level == 4)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().RroomLv4;

                    }
                }
                if (room[i].type == 4)
                {
                    if (room[i].level == 1)
                    {
                        //BuildMenuScript.licz = 0;
                    }
                    if (room[i].level == 2)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().YroomLv2;
                    }
                    if (room[i].level == 3)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().YroomLv3;

                    }
                    if (room[i].level == 4)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().YroomLv4;

                    }
                }
                if (room[i].type == 5)
                {
                    if (room[i].level == 1)
                    {
                        //BuildMenuScript.licz = 0;
                    }
                    if (room[i].level == 2)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().ProomLv2;
                    }
                    if (room[i].level == 3)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().ProomLv3;

                    }
                    if (room[i].level == 4)
                    {
                        GameObject.Find(room[i].name).GetComponent<SpriteRenderer>().sprite = GameObject.Find("mainObject").GetComponent<GameConstants>().ProomLv4;

                    }
                }
                try
                {
                    GameObject.Find(room[i].name).GetComponent<UpgradeMenuScript>().roomLvl = room[i].level;
                }
                catch
                {
                    print("fail");
                }
            }
           /* print(room[1].name);
            print(room[1].level);
            print(room[1].type);
            print(room[1].upgrading);
            print(room[1].x);
            print(room[1].y);*/



            print("Load Complete!");
        }
        else
        {
            print("nie ma!");
        }

        if (File.Exists(Application.dataPath + "/playerData.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.dataPath + "/playerData.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);

            GameConstants.Money = (int)data.money[0];
            GameConstants.RMoney = data.money[1];
            GameConstants.YMoney = data.money[2];
            GameConstants.PMoney = data.money[3];
            GameConstants.RoomCount = (int)data.RoomCount;
            GameConstants.RRoomCount = data.RRoomCount;
            GameConstants.YRoomCount = data.YRoomCount;
            GameConstants.PRoomCount = data.PRoomCount;
            GameConstants.MagazineCount = data.MagazineCount;
            GameConstants.buildLimit = data.buildLimit;
            GameConstants.AllRoomCount = data.AllRoomCount;
            GameConstants.mainCost = data.mainCost;
            GameConstants.whiteCost1 = data.whiteCost1;
            GameConstants.redCost1 = data.redCost1;
            GameConstants.redCost2 = data.redCost2;
            GameConstants.yellowCost1 = data.yellowCost1;
            GameConstants.yellowCost2 = data.yellowCost2;
            GameConstants.purpleCost1 = data.purpleCost1;
            GameConstants.purpleCost2 = data.purpleCost2;

            if (data.building == true)
            {
               // print("Load Buikl");
                Main.counter = true;
                Main.building = true;
                Main.aftcounter = false;
                Main.EndTime = data.EndTime;
                Main.StartTime = data.StartTime;
                Main.CorTime = data.ConTime;


                Main.buildingInProgress = Instantiate(GameObject.Find("mainObject").GetComponent<GameConstants>().banner
                    , new Vector3(data.bannerX, data.bannerY, data.bannerZ), Quaternion.identity) as GameObject;

                Main.clone = Instantiate(GameObject.Find("mainObject").GetComponent<GameConstants>().timeText,
                    new Vector3(data.timeTextX, data.timeTextY, data.timeTextZ), Quaternion.identity) as GameObject;

                Main.X = data.X;
                Main.Y = data.Y;
                BuildMenuScript.licz = data.licz;


            }

            print("Load Complete!");
        }
    }

    

}

[Serializable]
class PlayerData
{
    public float[] money = new float[4];
    public float RoomCount;
    public float RRoomCount;
    public float YRoomCount;
    public float PRoomCount;
    public int MagazineCount=1;
    public int AllRoomCount = 0;
    public int buildLimit = 0;

    public int mainCost = 0;
    public  int whiteCost1 = 0;
    public  int redCost1 = 0;
    public  int redCost2 = 0;
    public  int yellowCost1 = 0;
    public  int yellowCost2 = 0;
    public  int purpleCost1 = 0;
    public  int purpleCost2 = 0;

    public bool building=false;
    public int StartTime=0;
    public int EndTime=0;
    public int ConTime=0;

    public float bannerX = 0;
    public float bannerY = 0;
    public float bannerZ = 0;

    public float timeTextX = 0;
    public float timeTextY = 0;
    public float timeTextZ = 0;

    public int X = 0;
    public int Y = 0;
    public int licz = 0;





}

[Serializable]
class RoomInfo
{
    public int level;
    public int type;
    public string name;
    public int x;
    public int y;
    //public Vector2 position;
    public bool upgrading;
    //  public bool building;
    // public bool timeLeft;

}

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}