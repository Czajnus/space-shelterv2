﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : Room {

    public int y;

    public Elevator(GameObject go) : base(go) {

    }

    public Elevator(GameObject go, int y) : base(go, 1, y) {

    }

}
