﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgMoves : MonoBehaviour {
    public float speed;
	
    void Start()
    {
    }
	// Update is called once per frame
	void Update () {
        if (transform.position.x <= 24f)
        {
            transform.position = new Vector3(transform.position.x + speed, transform.position.y, transform.position.z);
            
        }
        else
            transform.position = new Vector3(-28f, 1.5f, transform.position.z);
	}
}
