﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorBuildScript : MonoBehaviour {

    public void OnMouseDown() {
        var room = GameObject.Find("mainObject").GetComponent<Main>().RoomByGameObject(gameObject);
        GameObject.Find("mainObject").GetComponent<Main>().ClearGreen();
        GameObject.Find("mainObject").GetComponent<Main>().AddElevator(room.y, false);
    }

}
