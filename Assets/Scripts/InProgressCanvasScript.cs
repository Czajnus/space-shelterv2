﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using System;

public class InProgressCanvasScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
        GameObject.Find("InProgressCanvas").GetComponent<Canvas>().enabled = false;
        GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
        

        }

    public IEnumerator StopDrone()
    {

        yield return new WaitForSeconds(30f);
        print("STOP DRONE!!");
        GameConstants.buyDrone2 = false;
        GameConstants.buyDrone = false;
        try
        {
            Destroy(GameObject.Find("Drone(Clone)"));
            Destroy(GameObject.Find("DroneLaser(Clone)"));
        }
        catch { }

    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                GameConstants.buyDrone = true;
                GameConstants.buyDrone2 = true;
                GameObject banner= GameObject.Find("banner(Clone)");
                
                Vector3 pos = new Vector3(GameObject.Find("banner(Clone)").transform.position.x, GameObject.Find("banner(Clone)").transform.position.y, GameObject.Find("banner(Clone)").transform.position.z);
                Instantiate(GameObject.Find("mainObject").GetComponent<GameConstants>().drone, new Vector3(banner.transform.position.x, banner.transform.position.y+1f,banner.transform.position.z-2), Quaternion.identity);
                GameObject drone = GameObject.Find("Drone(Clone)");
                Instantiate(GameObject.Find("mainObject").GetComponent<GameConstants>().droneLaser, new Vector3(drone.transform.position.x, drone.transform.position.y - .5f, drone.transform.position.z), Quaternion.identity);
                StartCoroutine(StopDrone());
                StopDrone();
                //Main.EndTime = Main.EndTime-(Main.EndTime-Main.CurrentTime)/2;
                /* Main.ConstructionEndTime = Main.ConstructionEndTime.Subtract(
                     new TimeSpan((Main.ConstructionEndTime - DateTime.Now).Ticks / 2));*/
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    public void CloseButton()
    {
        GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
        GameObject.Find("InProgressCanvas").GetComponent<Canvas>().enabled = false;

    }

    public void StopBuilding()
    {
        if (BuildMenuScript.licz == 1)
        {
            GameConstants.Money += 10;
        }
        else if (BuildMenuScript.licz == 2)
        {
            GameConstants.RMoney += 20;
            GameConstants.Money += 50;
        }
        else if (BuildMenuScript.licz == 3)
        {
            GameConstants.YMoney += 40;
            GameConstants.Money += 100;
        }
        else if (BuildMenuScript.licz == 4)
        {
            GameConstants.PMoney += 60;
            GameConstants.Money += 200;
        }
        Destroy(GameObject.Find("banner(Clone)"));
        Destroy(GameObject.Find("Text(Clone)"));
        Destroy(GameObject.Find("BuildRoom(Clone)"));
        Main.aftcounter = false;
        Main.counter = false;
        Main.building = false;
        Main.warningCounter = false;
        GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
        GameObject.Find("InProgressCanvas").GetComponent<Canvas>().enabled = false;

    }

    public void BuyDrone()
    {

    }
}
