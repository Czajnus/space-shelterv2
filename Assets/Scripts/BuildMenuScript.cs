﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildMenuScript : MonoBehaviour {

    public GameObject buildMenu;
    public static int licz = 0;

	// Use this for initialization
	void Start () {
        buildMenu = GameObject.Find("BuildMenu");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    // FUNKCJE WYWOLYWANE PRZEZ KLIKNIECIE BUTTONA DANIEGO ROOMU. WARUNKI NA HAJS
    public void BuildBasicRoom() {

        if (GameConstants.AllRoomCount < GameConstants.buildLimit)
        {
            if (GameConstants.Money >= 10)      //WARUNEK OD HAJSU
            {
                GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
                buildMenu.GetComponent<Canvas>().enabled = false;
                if (Main.building == false)
                {
                    licz = 1;


                    GameObject.Find("mainObject").GetComponent<BuildingState>().buildingType = typeof(BasicRoom);
                    GameObject.Find("mainObject").GetComponent<Main>().AddGreen();
                    // GameConstants.Money -= 10;
                }
                else
                {
                    GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Building in progress. Please wait!";
                    Main.warningCounter = true;
                    Main.warnTime = 2;
                    //Main.warnTime = 5;
                }
            }
        }
        else
        {
            GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Build limit reached. Upgrade any room to +3";
            Main.warningCounter = true;
            Main.warnTime = 2;
        }
    }
    public void BuildRedRoom()
    {
        if (GameConstants.AllRoomCount < GameConstants.buildLimit)
        {
            if (GameConstants.Money >= 50 && GameConstants.RMoney >= 20)
            {
                GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
                buildMenu.GetComponent<Canvas>().enabled = false;
                if (Main.building == false)
                {
                    licz = 2;


                    GameObject.Find("mainObject").GetComponent<BuildingState>().buildingType = typeof(BasicRoom);
                    GameObject.Find("mainObject").GetComponent<Main>().AddGreen();
                    // GameConstants.Money -= 20;
                }
                else
                {
                    GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Building in progress. Please wait!";
                    Main.warningCounter = true;
                    Main.warnTime = 2;
                    //  Main.warnTime = 2;
                }
            }
        }
        else
        {
            GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Build limit reached. Upgrade any room to +3";
            Main.warningCounter = true;
            Main.warnTime = 2;
        }
    }
    public void BuildYellowRoom()
    {
        if (GameConstants.AllRoomCount < GameConstants.buildLimit)
        {
            if (GameConstants.Money >= 100 && GameConstants.YMoney >= 40)
            {
                GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
                buildMenu.GetComponent<Canvas>().enabled = false;
                if (Main.building == false)
                {
                    licz = 3;



                    GameObject.Find("mainObject").GetComponent<BuildingState>().buildingType = typeof(BasicRoom);
                    GameObject.Find("mainObject").GetComponent<Main>().AddGreen();
                }
                else
                {
                    GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Building in progress. Please wait!";
                    Main.warningCounter = true;
                    Main.warnTime = 2;
                    // Main.warnTime = 5;
                }

                // GameConstants.Money -= 30;
            }
        }
        else
        {
            GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Build limit reached. Upgrade any room to +3";
            Main.warningCounter = true;
            Main.warnTime = 2;
        }
    }
    public void BuildPurpleRoom()
    {
        if (GameConstants.AllRoomCount < GameConstants.buildLimit)
        {
            if (GameConstants.Money >= 200 && GameConstants.PMoney >= 60)
            {
                GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
                buildMenu.GetComponent<Canvas>().enabled = false;
                if (Main.building == false)
                {
                    licz = 4;
                    GameObject.Find("mainObject").GetComponent<BuildingState>().buildingType = typeof(BasicRoom);
                    GameObject.Find("mainObject").GetComponent<Main>().AddGreen();
                }
                else
                {
                    GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Building in progress. Please wait!";
                    Main.warningCounter = true;
                    Main.warnTime = 2;
                    //Main.warnTime = 5;
                }
                // GameConstants.Money -= 40;
            }
        }
        else
        {
            GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Build limit reached. Upgrade any room to +3";
            Main.warningCounter = true;
            Main.warnTime = 2;
        }
    }

    public void BuildElevator() {
        if (GameConstants.Money >= 15)
        {
            GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
            buildMenu.GetComponent<Canvas>().enabled = false;
            if (Main.building == false)
            {
                licz = 5;
                
                
                GameObject.Find("mainObject").GetComponent<BuildingState>().buildingType = typeof(Elevator);
                GameObject.Find("mainObject").GetComponent<Main>().AddGreenElevator();
                //GameConstants.Money -= 15;
            }
            else
            {
                GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Building in progress. Please wait!";
                Main.warningCounter = true;
                Main.warnTime = 2;
                // Main.warnTime = 5;
            }
        }
    }

    public void CloseCanvas()
    {
        GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
        buildMenu.GetComponent<Canvas>().enabled = false;
    }

}
