﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicRoom : Room {

    public BasicRoom(GameObject go) : base(go) {

    }

    public BasicRoom(GameObject go, int x, int y, string name) : base(go, x, y) {
        this.gameObject.name = name;
    }

}
