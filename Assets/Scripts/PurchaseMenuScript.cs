﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseMenuScript : MonoBehaviour {

    private GameObject purchaseMenu;

    private void Start()
    {
       purchaseMenu = GameObject.Find("PurchaseMenu");
       
    }

    public void ShowBuyMenu()
    {
        if (GameObject.Find("mainObject").GetComponent<GameConstants>().click == false)
        {
            purchaseMenu.GetComponent<Canvas>().enabled = true;
            GameObject.Find("mainObject").GetComponent<GameConstants>().click = true;
        }
    }

}
