﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Advertisements;

public class UpgradeMenuScript : MonoBehaviour
{

    public GameObject upgradeMenu;
    public GameObject room;
    private GameObject panelText;
    private GameObject engeneer;
    private GameObject scientist;
    private Sprite roomSprite;
    private GameObject clone;
    public GameObject mainObject;
    private GameConstants gameConstants;

    public Sprite WroomLv2;
    public Sprite WroomLv3;
    public Sprite WroomLv4;

    private string RoomName;
    private float X;
    private float Y;
    public bool upgrading = false;
    // public bool building = false;
    private bool counter = false;
    private bool aftUpgrade = false;


    public int roomKind;
    public int roomLvl = 1;

    private bool warningCounter = false;
    private float warningTime;
    private float timeL = 0;
    // Use this for initialization
    void Start()
    {

        upgradeMenu = GameObject.Find("UpgradeCanvas");
        room = GameObject.Find("RoomSprite");
        panelText = GameObject.Find("CostText");
        gameConstants = mainObject.GetComponent<GameConstants>();

    }

    public void OnMouseDown()
    {
        if (GameObject.Find("mainObject").GetComponent<GameConstants>().click == false)
        {
            if (GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading == false)
            {

                GameConstants.roomX = this.gameObject.transform.position.x;
                GameConstants.roomY = this.gameObject.transform.position.y;
                gameConstants.RoomName = "";
                GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName = this.gameObject.name;

                GameObject.Find("mainObject").GetComponent<GameConstants>().click = true;

                // WARUNKI NA WYPELNIENIE TEXT PANELU W UPGRADE CANVAS.

                if (this.gameObject.GetComponent<UpgradeMenuScript>().roomKind == 1)
                {
                    GameObject.Find("mainObject").GetComponent<GameConstants>().RoomKind = 0;
                    panelText.GetComponent<UnityEngine.UI.Text>().text = "Cost: "+GameConstants.mainCost+"pts";
                }
                else if (this.gameObject.GetComponent<UpgradeMenuScript>().roomKind == 2)
                {

                    GameObject.Find("mainObject").GetComponent<GameConstants>().RoomKind = 1;
                    panelText.GetComponent<UnityEngine.UI.Text>().text = "Cost: " + GameConstants.whiteCost1 + "pts";

                }
                else if (this.gameObject.GetComponent<UpgradeMenuScript>().roomKind == 3)
                {
                    GameObject.Find("mainObject").GetComponent<GameConstants>().RoomKind = 2;
                    panelText.GetComponent<UnityEngine.UI.Text>().text = "Cost: "+GameConstants.redCost1+"pts &"+GameConstants.redCost2+"rts";
                }
                else if (this.gameObject.GetComponent<UpgradeMenuScript>().roomKind == 4)
                {
                    GameObject.Find("mainObject").GetComponent<GameConstants>().RoomKind = 3;
                    panelText.GetComponent<UnityEngine.UI.Text>().text = "Cost: "+GameConstants.yellowCost1+"pts &"+GameConstants.yellowCost2+"yts";
                }
                else if (this.gameObject.GetComponent<UpgradeMenuScript>().roomKind == 5)
                {
                    GameObject.Find("mainObject").GetComponent<GameConstants>().RoomKind = 4;
                    panelText.GetComponent<UnityEngine.UI.Text>().text = "Cost: "+GameConstants.purpleCost1+"pts &"+GameConstants.purpleCost2+"rts";
                }
                else
                    panelText.GetComponent<UnityEngine.UI.Text>().text = "Error";

                //  print(this.gameObject.name);
                roomSprite = this.gameObject.GetComponent<SpriteRenderer>().sprite;
                room.GetComponent<SpriteRenderer>().enabled = true;
                room.GetComponent<SpriteRenderer>().sprite = roomSprite;
                upgradeMenu.GetComponent<Canvas>().enabled = true;
            }
            else
            {
                GameObject.Find("InUpdProgressCanvas").GetComponent<Canvas>().enabled = true;
                GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Upgrading in progress. Please wait!";
                warningCounter = true;
                warningTime = 2;
            }
        }

    }

    public void CloseButton()
    {
        GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
        GameObject.Find("InUpdProgressCanvas").GetComponent<Canvas>().enabled = false;
    }

    public void StopUpgrading()
    {
        if (GameObject.Find("mainObject").GetComponent<GameConstants>().RoomKind == 1)
        {
            GameConstants.Money += 500;
        }
        else if (GameObject.Find("mainObject").GetComponent<GameConstants>().RoomKind == 2)
        {
            GameConstants.Money += 500;
            GameConstants.RMoney += 200;
        }
        else if (GameObject.Find("mainObject").GetComponent<GameConstants>().RoomKind == 3)
        {
            GameConstants.Money += 1000;
            GameConstants.YMoney += 300;
        }
        else if (GameObject.Find("mainObject").GetComponent<GameConstants>().RoomKind == 4)
        {
            GameConstants.Money += 2000;
            GameConstants.PMoney += 500;
        }
        GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading = false;
        upgrading = false;
        aftUpgrade = false;
        counter = false;
        warningCounter = false;
        Destroy(GameObject.Find("Text(Clone)"));
        GameObject.Find("InUpdProgressCanvas").GetComponent<Canvas>().enabled = false;
    }

    public void CloseUpgrCanvas()
    {
        if (GameObject.Find("mainObject").GetComponent<GameConstants>().click == true)
        {
            GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
            upgradeMenu = GameObject.Find("UpgradeCanvas");
            room = GameObject.Find("RoomSprite");
            upgradeMenu.GetComponent<Canvas>().enabled = false;
            room.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    //Funkcja po nacisnieciu guzika Upgrade w UpgradeCanvas
    public void UpgradeButton()
    {
        //  print("Upgrade Button!");
        upgradeMenu = GameObject.Find("UpgradeCanvas");
        room = GameObject.Find("RoomSprite");
        panelText = GameObject.Find("CostText");
        engeneer = GameObject.Find("Engeneer");
        scientist = GameObject.Find("Scientist");

        //tego ifa olac bo jest od glownego budynku
        if (panelText.GetComponent<UnityEngine.UI.Text>().text == "Cost: "+GameConstants.mainCost+"pts")
        {
            //    print("Upg: " + upgrading + " aft: " + aftUpgrade + " cou: " + counter);
            if (GameConstants.Money >= GameConstants.mainCost)
            {
                if (upgrading == true)
                {
                    GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Upgrading in progress. Please wait!";
                    warningCounter = true;
                    warningTime = 2;
                }
                if (aftUpgrade == false && upgrading == false)
                {
                    upgrading = true;
                    clone = Instantiate(gameConstants.timeText, new Vector3(GameConstants.roomX, GameConstants.roomY, -1), Quaternion.identity) as GameObject;
                    GameObject.Find("UpgradeState").GetComponent<UnityEngine.UI.Text>().text = "Upgrading";
                    counter = true;
                    timeL = 10;
                    GameConstants.Money -= GameConstants.mainCost;
                    engeneer.transform.position = new Vector3(GameConstants.roomX, GameConstants.roomY - .09f, -1);
                }
            }
            else if (GameConstants.Money <= GameConstants.mainCost && aftUpgrade == false)
            {
               // GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Not enough money!";
                warningCounter = true;
                warningTime = 2;
            }
            else if (counter == false)
            {
                //    print("DONE!");
                upgrading = false;
                GameObject.Find("UpgradeState").GetComponent<UnityEngine.UI.Text>().text = " ";
                aftUpgrade = false;
                GameConstants.RoomCount += 1;
                GameConstants.buildLimit += 1;
                GameConstants.mainCost *= 2;
               // GameConstants.buildLimit += 1;
            }
            //  print("Upg: " + upgrading + " aft: " + aftUpgrade + " cou: " + counter);

        }

        // If od White Roomu. Sprawdza co jest na textPanelu w UpgradeCanvas
        if (panelText.GetComponent<UnityEngine.UI.Text>().text == "Cost: " + GameConstants.whiteCost1 + "pts")
        {
            //  print("Upg: " + upgrading + " aft: " + aftUpgrade + " cou: " + counter);
            if (GameConstants.Money >= GameConstants.whiteCost1)
            {
                if (upgrading == true || GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading == true)
                {
                    GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Upgrading in progress. Please wait!";
                    warningCounter = true;
                    warningTime = 2;
                }
                if (aftUpgrade == false && upgrading == false)
                {
                    upgrading = true;
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().upgrading = true;
                    GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading = true;
                    clone = Instantiate(gameConstants.timeText, new Vector3(GameConstants.roomX, GameConstants.roomY, -1), Quaternion.identity) as GameObject;
                    GameObject.Find("UpgradeState").GetComponent<UnityEngine.UI.Text>().text = "Upgrading";
                    counter = true;
                    timeL = 10;
                    GameConstants.Money -= GameConstants.whiteCost1;
                    engeneer.transform.position = new Vector3(GameConstants.roomX, GameConstants.roomY - .09f, -1);
                    engeneer.GetComponent<Animator>().SetBool("working", true);
                }
            }
            else if (GameConstants.Money <= GameConstants.whiteCost1 && aftUpgrade == false)
            {
                GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Not enough money!";
                warningCounter = true;
                warningTime = 2;
            }
            if (counter == false && aftUpgrade == true)
            {
                //  print("DONE!");
                upgrading = false;
                GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().upgrading = false;
                GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading = false;
                GameObject.Find("UpgradeState").GetComponent<UnityEngine.UI.Text>().text = " ";
                aftUpgrade = false;
                //   print("Upgrade Const!: " + gameConstants.RoomName);
                //PODMIANA TEXTUR
                if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 1)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 2;
                    //  print("Room name : " + GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.WroomLv2;

                }
                else if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 2)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 3;
                    //  print("Room name : " + GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.WroomLv3;
                }
                else if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 3)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 4;

                    //  print("Room name : " + gameConstants.RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.WroomLv4;
                }
                engeneer.GetComponent<Animator>().SetBool("working", false);
                GameConstants.RoomCount += 1;
                GameConstants.whiteCost1 *= 2;
            }
            // print("Upg: " + upgrading + " aft: " + aftUpgrade + " cou: " + counter);

        }
        //RED ROOM UPGRADE
        if (panelText.GetComponent<UnityEngine.UI.Text>().text == "Cost: " + GameConstants.redCost1 + "pts &" + GameConstants.redCost2 + "rts")
        {
            //  print("Upg: " + upgrading + " aft: " + aftUpgrade + " cou: " + counter);
            if (GameConstants.Money >= GameConstants.redCost1 && GameConstants.RMoney >= GameConstants.redCost2)
            {
                if (upgrading == true || GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading == true)
                {
                    GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Upgrading in progress. Please wait!";
                    warningCounter = true;
                    warningTime = 2;
                }

                if (aftUpgrade == false && upgrading == false)
                {
                    upgrading = true;
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().upgrading = true;
                    GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading = true;
                    clone = Instantiate(gameConstants.timeText, new Vector3(GameConstants.roomX, GameConstants.roomY, -1), Quaternion.identity) as GameObject;
                    GameObject.Find("UpgradeState").GetComponent<UnityEngine.UI.Text>().text = "Upgrading";
                    counter = true;
                    timeL = 10;
                    GameConstants.Money -= GameConstants.redCost1;
                    GameConstants.RMoney -= GameConstants.redCost2;
                    engeneer.transform.position = new Vector3(GameConstants.roomX, GameConstants.roomY - .09f, -1);
                    engeneer.GetComponent<Animator>().SetBool("working", true);
                }
            }
            else if (GameConstants.Money <= GameConstants.redCost1 || GameConstants.RMoney <= GameConstants.redCost2 && aftUpgrade == false)
            {
                GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Not enough money!";
                warningCounter = true;
                warningTime = 2;
            }
            if (counter == false && aftUpgrade == true)
            {
                //  print("DONE!");
                GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading = false;
                GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().upgrading = false;
                upgrading = false;
                aftUpgrade = false;
                GameObject.Find("UpgradeState").GetComponent<UnityEngine.UI.Text>().text = " ";
                //PODMIANA TEXTUR
                if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 1)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 2;
                    //  print("Room name : " + GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.RroomLv2;
                }
                else if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 2)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 3;
                    //  print("Room name : " + GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.RroomLv3;
                }
                else if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 3)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 4;
                    //    print("Room name : " + GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.RroomLv4;
                }
                engeneer.GetComponent<Animator>().SetBool("working", false);
                GameConstants.RRoomCount += 1;
                GameConstants.redCost1 *= 2;
                GameConstants.redCost2 *= 2;
            }
            // print("Upg: " + upgrading + " aft: " + aftUpgrade + " cou: " + counter);
        }
        //Yellow Room Upgrade
        if (panelText.GetComponent<UnityEngine.UI.Text>().text == "Cost: " + GameConstants.yellowCost1 + "pts &" + GameConstants.yellowCost2 + "yts")
        {
            //  print("Upg: " + upgrading + " aft: " + aftUpgrade + " cou: " + counter);
            if (GameConstants.Money >= GameConstants.yellowCost1 && GameConstants.YMoney >= GameConstants.yellowCost2)
            {
                if (upgrading == true || GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading == true)
                {
                    GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Upgrading in progress. Please wait!";
                    warningCounter = true;
                    warningTime = 2;
                }

                if (aftUpgrade == false && upgrading == false)
                {
                    upgrading = true;
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().upgrading = true;
                    GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading = true;
                    clone = Instantiate(gameConstants.timeText, new Vector3(GameConstants.roomX, GameConstants.roomY, -1), Quaternion.identity) as GameObject;
                    GameObject.Find("UpgradeState").GetComponent<UnityEngine.UI.Text>().text = "Upgrading";
                    counter = true;
                    timeL = 10;
                    GameConstants.Money -= GameConstants.yellowCost1;
                    GameConstants.YMoney -= GameConstants.yellowCost2;
                    engeneer.transform.position = new Vector3(GameConstants.roomX, GameConstants.roomY - .09f, -1);
                    engeneer.GetComponent<Animator>().SetBool("working", true);
                }
            }
            else if (GameConstants.Money <= GameConstants.yellowCost1 || GameConstants.YMoney <= GameConstants.yellowCost2 && aftUpgrade == false)
            {
                GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Not enough money!";
                warningCounter = true;
                warningTime = 2;
            }
            if (counter == false && aftUpgrade == true)
            {
                // print("DONE!");
                upgrading = false;
                GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().upgrading = false;
                GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading = false;
                GameObject.Find("UpgradeState").GetComponent<UnityEngine.UI.Text>().text = " ";
                aftUpgrade = false;
                //PODMIANA TEXTUR
                if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 1)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 2;
                    // print("Room name : " + gameConstants.RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.YroomLv2;
                }
                else if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 2)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 3;
                    //  print("Room name : " + gameConstants.RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.YroomLv3;
                }
                else if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 3)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 4;
                    //  print("Room name : " + gameConstants.RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.YroomLv4;
                }
                engeneer.GetComponent<Animator>().SetBool("working", false);
                GameConstants.MagazineCount *= 2;
                GameConstants.YRoomCount += 1;
                GameConstants.yellowCost1 *= 2;
                GameConstants.yellowCost2 *= 2;
            }
            // print("Upg: " + upgrading + " aft: " + aftUpgrade + " cou: " + counter);

        }

        // PURPLE ROOM UPGRADE
        if (panelText.GetComponent<UnityEngine.UI.Text>().text == "Cost: " + GameConstants.purpleCost1 + "pts &" + GameConstants.purpleCost2 + "rts")
        {
            if (GameConstants.Money >= GameConstants.purpleCost1 && GameConstants.PMoney >= GameConstants.purpleCost2)
            {
                //  print("Upg: " + upgrading + " aft: " + aftUpgrade + " cou: " + counter);
                if (upgrading == true || GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading == true)
                {
                    GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Upgrading in progress. Please wait!";
                    warningCounter = true;
                    warningTime = 2;
                }

                if (aftUpgrade == false && upgrading == false)
                {
                    upgrading = true;
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().upgrading = true;
                    GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading = true;
                    clone = Instantiate(gameConstants.timeText, new Vector3(GameConstants.roomX, GameConstants.roomY, -1), Quaternion.identity) as GameObject;
                    GameObject.Find("UpgradeState").GetComponent<UnityEngine.UI.Text>().text = "Upgrading";
                    counter = true;
                    timeL = 10;
                    GameConstants.Money -= GameConstants.purpleCost1;
                    GameConstants.PMoney -= GameConstants.purpleCost2;
                    engeneer.transform.position = new Vector3(GameConstants.roomX, GameConstants.roomY - .09f, -1);
                    engeneer.GetComponent<Animator>().SetBool("working", true);
                }
            }
            else if (GameConstants.Money <= GameConstants.purpleCost1 || GameConstants.PMoney <= GameConstants.purpleCost2 && aftUpgrade == false)
            {
                GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Not enough money!";
                warningCounter = true;
                warningTime = 2;
            }
            if (counter == false && aftUpgrade == true)
            {
                //  print("DONE!");
                upgrading = false;
                GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().upgrading = false;
                GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading = false;
                aftUpgrade = false;
                GameObject.Find("UpgradeState").GetComponent<UnityEngine.UI.Text>().text = " ";

                //PODMIANA TEXTUR
                if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 1)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 2;
                    //   print("Room name : " + gameConstants.RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.ProomLv2;
                }
                else if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 2)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 3;
                    //    print("Room name : " + gameConstants.RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.ProomLv3;
                }
                else if (GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl == 3)
                {
                    GameObject.Find(GameObject.Find("mainObject").GetComponent<GameConstants>().RoomName).GetComponent<UpgradeMenuScript>().roomLvl = 4;
                    //     print("Room name : " + gameConstants.RoomName);
                    GameObject.Find(gameConstants.RoomName).GetComponent<SpriteRenderer>().sprite = gameConstants.ProomLv4;
                }
                GameConstants.PRoomCount += 1;
                GameConstants.purpleCost1 *= 2;
                GameConstants.purpleCost2 *= 2;
                engeneer.GetComponent<Animator>().SetBool("working", false);
            }
            //   print("Upg: " + upgrading + " aft: " + aftUpgrade + " cou: " + counter);
            // }
        }
        GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
        upgradeMenu.GetComponent<Canvas>().enabled = false;
        room.GetComponent<SpriteRenderer>().enabled = false;
        // print("Upgrade Button end");
    }
    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
        GameObject.Find("mainObject").GetComponent<GameConstants>().click = false;
        GameObject.Find("InUpdProgressCanvas").GetComponent<Canvas>().enabled = false;

    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                timeL = timeL / 2;
                //print("timeL after ADS:" + timeL);
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
    void Update()
    {
        if (counter == true)
        {
            timeL -= Time.deltaTime;
            clone.GetComponent<TextMesh>().text = "Left: " + Math.Round(timeL);

            if (timeL < 0)
            {
                counter = false;
                aftUpgrade = true;
                upgrading = false;
                GameObject.Find("mainObject").GetComponent<GameConstants>().upgrading = false;

                // print("Counting!!: " + timeL);
                UpgradeButton();
                // GameObject.Find("UpgradeState").GetComponent<UnityEngine.UI.Text>().text = " ";
                Destroy(clone);

            }
        }
        if (warningCounter == true)
        {
            warningTime -= Time.deltaTime;
            //print(warningTime);
            if (warningTime < 0)
            {
                GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = " ";
                warningCounter = false;

            }
        }

    }
}
