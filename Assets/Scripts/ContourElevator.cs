﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContourElevator : Elevator {

    public ContourElevator(GameObject go) : base(go) {

    }

    public ContourElevator(GameObject go, int y) : base(go, y) {

    }

}
