﻿using UnityEngine;
using System;

namespace Assets {/*
    public class Person {

        public static GameObject prefab;

        private GameObject gameObject;

        private Vector2 currentRoom;

        private float nextPositionX;

        private float speed = .01f;

        public Person(int x, int y, GameObject go) {
            gameObject = go;
            currentRoom = new Vector2(x, y);
            gameObject.transform.position = new Vector3(
                UnityEngine.Random.Range(-GameConstants.RoomSize.x/3,GameConstants.RoomSize.x/3)+ GameConstants.BaseRoomPosition.x,
            GameConstants.BaseRoomPosition.y - GameConstants.RoomSize.y / 4, -1);
            if (GameConstants.BaseRoomPosition.x > gameObject.transform.position.x) {
                nextPositionX = GameConstants.BaseRoomPosition.x + GameConstants.RoomSize.x / 3
                    - UnityEngine.Random.Range(0, GameConstants.RoomSize.x / 6);
            }
            else {
                nextPositionX = GameConstants.BaseRoomPosition.x - GameConstants.RoomSize.x / 3
                   + UnityEngine.Random.Range(0, GameConstants.RoomSize.x / 6);
            }
        }

        private DateTime LastDirectionUpdate = DateTime.Now;

        public void Update() {
            if ((DateTime.Now - LastDirectionUpdate).TotalSeconds > 2) {
                if (UnityEngine.Random.value > .9) {
                    if (GameConstants.BaseRoomPosition.x > gameObject.transform.position.x) {
                        nextPositionX = GameConstants.BaseRoomPosition.x + 
                            GameConstants.RoomSize.x / 3 - UnityEngine.Random.Range(0,GameConstants.RoomSize.x / 6) ;
                    }
                    else {
                        nextPositionX = GameConstants.BaseRoomPosition.x - GameConstants.RoomSize.x / 3 
                            + UnityEngine.Random.Range(0, GameConstants.RoomSize.x / 6) ;
                    }
                }
                LastDirectionUpdate = DateTime.Now;
            }

            if (nextPositionX != gameObject.transform.position.x) {
                if (nextPositionX < gameObject.transform.position.x) {
                    gameObject.transform.position = new Vector3(Mathf.Max(gameObject.transform.position.x - speed, nextPositionX),
                        gameObject.transform.position.y, gameObject.transform.position.z);
                }
                else {
                    gameObject.transform.position = new Vector3(Mathf.Min(gameObject.transform.position.x + speed, nextPositionX),
                        gameObject.transform.position.y, gameObject.transform.position.z);
                }
            }
        }

    }*/
}
