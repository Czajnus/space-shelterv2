﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

namespace Assets {
    public class Quest {

        [Serializable]
        public class QuestJson {
            public int id;
            public string type;
            public string color;
            public int count;
            public string rewardColor;
            public int rewardAmount;

            public QuestJson() {

            }
        }

        public enum QuestType {
            BUILD,
            GATHER,
            UPGRADE
        };

        public enum Color {
            WHITE,
            RED,
            YELLOW,
            PURPLE,
            ANY
        };

        Dictionary<string, QuestType> questTypeByString = new Dictionary<string, QuestType>() {
            { "build", QuestType.BUILD },
            { "gather", QuestType.GATHER },
            { "upgrade", QuestType.UPGRADE }
        };

        Dictionary<string, Color> colorByString = new Dictionary<string, Color>() {
            { "white", Color.WHITE },
            { "red", Color.RED },
            { "yellow", Color.YELLOW },
            { "purple", Color.PURPLE },
            { "any", Color.ANY }
        };

        public int id;
        public QuestType type;
        public Color color;
        public int currentCount;
        public int count;
        public Color rewardColor;
        public int rewardAmount;

        public Quest(int id, string type, string color, int count, string rewardColor, int rewardAmount) {
            this.id = id;
            this.type = questTypeByString[type];
            this.color = colorByString[color];
            this.count = count;
            this.rewardColor = colorByString[rewardColor];
            this.rewardAmount = rewardAmount;
            this.currentCount = 0;
        }

        public Quest(QuestJson qj) : this(
            qj.id, qj.type, qj.color, qj.count, qj.rewardColor, qj.rewardAmount) {
        }

        public void FinishQuest() {
            GameObject.Find("Qstatus").GetComponent<UnityEngine.UI.Text>().text = "Status: Finished!";
            GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = "Quest Finished!!";
            Main.warnTime = 3;
            Main.warningCounter = true;
            GameConstants.questFlag = true;
            switch (rewardColor) {
                case Color.WHITE:
                    GameConstants.Money += rewardAmount;
                    break;
                case Color.RED:
                    GameConstants.RMoney += rewardAmount;
                    break;
                case Color.YELLOW:
                    GameConstants.YMoney += rewardAmount;
                    break;
                case Color.PURPLE:
                    GameConstants.PMoney += rewardAmount;
                    break;
            }
            Main.questList.Remove(this);
        }

        public void AddCount() {
            currentCount++;
            Debug.Log(String.Format( "Quest completion: {0}/{1}", currentCount, count));
            if (currentCount == count) {
                Debug.Log("Building quest completed");
                FinishQuest();
            }
        }

        public void Check() {
            switch (color) {
                case Color.WHITE:
                    if (GameConstants.Money >= count) {
                        FinishQuest();
                        Debug.Log("Money quest completed");
                    }
                    break;
                case Color.RED:
                    if (GameConstants.RMoney >= count) {
                        FinishQuest();
                        Debug.Log("Money quest completed");
                    }
                    break;
                case Color.YELLOW:
                    if (GameConstants.YMoney >= count) {
                        FinishQuest();
                        Debug.Log("Money quest completed");
                    }
                    break;
                case Color.PURPLE:
                    if (GameConstants.PMoney >= count) {
                        FinishQuest();
                        Debug.Log("Money quest completed");
                    }
                    break;
            }
        }

    }
}
