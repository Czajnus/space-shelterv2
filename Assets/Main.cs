﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

using Assets;

public class Main : MonoBehaviour
{

    public GameObject mainObject;
    public GameConstants gameConstants;
    public Dictionary<int, List<Room>> floors = new Dictionary<int, List<Room>>();
    public static GameObject clone;
    public static GameObject buildingInProgress;
    private GameObject camera;
    public static bool counter = false;
    public static bool aftcounter = false;
    public static bool building = false;
    public static bool warningCounter = false;
    public static float timeL;
    private float timeE;
    public static float warnTime;
    public static int X;
    public static int Y;
    private GameObject progressBar;


    public static List<Quest> questList = new List<Quest>();


    private bool rob=false;

    private int Qrob = 1;
    



    private DateTime LastMoneyUpdate = DateTime.Now;

    public static DateTime ConstructionStartTime;
    public static DateTime ConstructionEndTime;

    public static int StartTime;
    public static int EndTime;
    public static int CurrentTime;
    public static int CorTime;

    private WWW www;

    int BuildingTime(int x, int minTime, int maxTime, int cnt)
    {
        /* int wynik = 0;
         float Fminuty = 0.0f;
         int sec = minTime + (int)((maxTime - minTime) * Math.Abs(1 - Math.Log10(9 / (double)(cnt - 1) * Math.Abs(cnt - 1 - x) + 1)));
         int Iminuty = (minTime + (int)((maxTime - minTime) * Math.Abs(1 - Math.Log10(9 / (double)(cnt - 1) * Math.Abs(cnt - 1 - x) + 1)))) / 60;

         int godziny = Iminuty / 60;
         int minuty = ((sec / 3600) - (Iminuty / 60)) * 60;
         Fminuty = (minTime + (int)((maxTime - minTime) * Math.Abs(1 - Math.Log10(9 / (double)(cnt - 1) * Math.Abs(cnt - 1 - x) + 1)))) / 60.0f;
         print("Godziny"+ Iminuty/60);
         print("Minuty "+ ((sec/3600)-(Iminuty/60))*60);
         print("Sekundy: "+(Fminuty - Iminuty) *60);
         print("TotalSec: " + (minTime + (int)((maxTime - minTime) * Math.Abs(1 - Math.Log10(9 / (double)(cnt - 1) * Math.Abs(cnt - 1 - x) + 1)))));

         */
        CorTime = minTime + (int)((maxTime - minTime) * Math.Abs(1 - Math.Log10(9 / (double)(cnt - 1) * Math.Abs(cnt - 1 - x) + 1)));
        EndTime = StartTime + CorTime;
        print("StartTime: " + StartTime);
        print("EndTime: " + EndTime);
        print("ConTime: " + CorTime);

        return minTime + (int)((maxTime - minTime) * Math.Abs(1 - Math.Log10(9 / (double)(cnt - 1) * Math.Abs(cnt - 1 - x) + 1)));
    }

    public void AddRoomByPosition(float x, float y)
    {
        AddRoom((int)((transform.position.x - gameConstants.BaseRoomPosition.x) / gameConstants.RoomSize.x),
            (int)((transform.position.y - gameConstants.BaseRoomPosition.y) / gameConstants.RoomSize.y), false);
    }

    public void AddRoom(int x, int y, bool contour)
    {
      //  print(counter + "   aft;" + aftcounter);
        //print("AddRoom!!!");
        if (!floors.ContainsKey(y))
        {
            var list = new List<Room>();
            for (int i = 0; i < 5; i++)
            {
                list.Add(null);
            }
            floors.Add(y, list);
        }
        if (contour == false)
        {
            //print("Contour: false");
            //
            //
            //WARUNKI ZLEZNE OD KLIKNIECIA W GUZIK BUDYNKU

            if (BuildMenuScript.licz == 0 && mainObject.GetComponent<GameConstants>().click == false)
            {
                floors[y][x] = new BasicRoom(Instantiate((contour ? gameConstants.contourPrefab : gameConstants.roomPrefab),
                    new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                    + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0)
                    ,
                    gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), Quaternion.identity), x, y, "MainRoom");
                GameConstants.RoomCount += 1;
                GameConstants.AllRoomCount += 1;
                BuildMenuScript.licz = 6;

            }

            if (BuildMenuScript.licz == 1 && mainObject.GetComponent<GameConstants>().click == false)
            {
                if (aftcounter == false)
                {
                    building = true;
                    clone = Instantiate(gameConstants.timeText, new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                   + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0) - .25f
                   ,
                   (gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y) + .34f, -3), Quaternion.identity) as GameObject;
                    buildingInProgress = Instantiate(gameConstants.banner,
                        new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                   + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0),
                   (gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), -1),
                        Quaternion.identity) as GameObject;

                    // print("counter=true   aftcounter=false");
                    counter = true;
                    //  timeL = 60*GameConstants.WhiteRoomC;

                    /*timeL = BuildingTime(Math.Min(GameConstants.WhiteRoomC - 1, 25), 30, 3600, 25);
                    timeE = 0;*/
                    ConstructionStartTime = DateTime.Now;

                    StartTime = CurrentTime;
                    ConstructionEndTime = DateTime.Now.AddSeconds(BuildingTime(Math.Min(GameConstants.WhiteRoomC - 1, 25), 30, 3600, 25));
                    GameConstants.Money -= 10;

                    progressBar = Instantiate(gameConstants.contourPrefab,
                        Vector3.zero
                        , Quaternion.identity) as GameObject;
                    progressBar.transform.localScale -= new Vector3(.2f, .7f);
                    progressBar.transform.position = new Vector3(buildingInProgress.transform.position.x - buildingInProgress.transform.localScale.x * .45f,
                        buildingInProgress.transform.position.y + buildingInProgress.transform.localScale.y * .2f,
                        -2);
                }
                //clone.GetComponent<TextMesh>().text = "120";
                if (counter == false)
                {
                    building = false;
                    //  print("counter=false");
                    floors[y][x] = new BasicRoom(Instantiate((contour ? gameConstants.contourPrefab : gameConstants.WroomPrefab),
                        new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                        + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0)
                        ,
                        gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), Quaternion.identity), x, y, "White Room" + GameConstants.RoomCount);
                    //
                    // ODEJMOWANIE HAJSU I DODAWANIE WARTOSCI DO LICZBY ROOMOW ZEBY HAJS SIE ZWIEKSZAL

                    GameConstants.RoomCount += 1;
                    GameConstants.RRoomCount += .3f;
                    GameConstants.YRoomCount += .2f;
                    GameConstants.PRoomCount += .1f;
                    GameConstants.WhiteRoomC += 1;
                    GameConstants.AllRoomCount += 1;

                    questList.Where(q => q.type == Quest.QuestType.BUILD &&
                                    (q.color == Quest.Color.WHITE || q.color == Quest.Color.ANY))
                                    .ToList().ForEach(q => q.AddCount());

                    BuildMenuScript.licz = 6;
                    aftcounter = false;
                }
                //clone.transform.position = new Vector3(0,0,0);
            }
            else if (BuildMenuScript.licz == 2 && mainObject.GetComponent<GameConstants>().click == false)
            {
                if (aftcounter == false)
                {
                    building = true;
                    clone = Instantiate(gameConstants.timeText, new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                   + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0) - .25f
                   ,
                   (gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y) + .34f, -3), Quaternion.identity) as GameObject;

                    buildingInProgress = Instantiate(gameConstants.banner, new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                   + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0),
                   (gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), -1), Quaternion.identity) as GameObject;

                    //print("counter=true   aftcounter=false");
                    counter = true;
                    //   timeL = 80 * GameConstants.RedRoomC;
                    /*   timeL = BuildingTime(Math.Min(GameConstants.RedRoomC - 1, 15), 60, 14400, 15);
                       timeE = 0;*/

                    ConstructionStartTime = DateTime.Now;
                    StartTime = CurrentTime;
                    ConstructionEndTime = DateTime.Now.AddSeconds(BuildingTime(Math.Min(GameConstants.RedRoomC - 1, 15), 60, 14400, 15));
                    GameConstants.RMoney -= 20;
                    GameConstants.Money -= 50;

                    progressBar = Instantiate(gameConstants.contourPrefab,
                        new Vector3(buildingInProgress.transform.position.x - buildingInProgress.transform.localScale.x * .45f,
                        buildingInProgress.transform.position.y + buildingInProgress.transform.localScale.y * .2f,
                       -2)
                        , Quaternion.identity) as GameObject;
                    progressBar.transform.localScale -= new Vector3(.2f, .7f);
                }
                if (counter == false)
                {
                    building = false;
                    floors[y][x] = new BasicRoom(Instantiate((contour ? gameConstants.contourPrefab : gameConstants.RroomPrefab),
                        new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                        + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0)
                        ,
                        gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), Quaternion.identity), x, y, "Red Room" + GameConstants.RRoomCount);

                    GameConstants.RRoomCount += 1;
                    GameConstants.RedRoomC += 1;
                    GameConstants.AllRoomCount += 1;
                    BuildMenuScript.licz = 6;

                    questList.Where(q => q.type == Quest.QuestType.BUILD &&
                                    (q.color == Quest.Color.RED || q.color == Quest.Color.ANY))
                                    .ToList().ForEach(q => q.AddCount());

                    aftcounter = false;
                }
            }
            else if (BuildMenuScript.licz == 3 && mainObject.GetComponent<GameConstants>().click == false)
            {
                if (aftcounter == false)
                {
                    building = true;
                    clone = Instantiate(gameConstants.timeText, new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                   + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0) - .25f
                   ,
                   (gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y) + .34f, -3), Quaternion.identity) as GameObject;

                    buildingInProgress = Instantiate(gameConstants.banner, new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                   + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0),
                   (gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), -1), Quaternion.identity) as GameObject;

                    // print("counter=true   aftcounter=false");
                    counter = true;
                    //   timeL = 100*GameConstants.MagazineCount;
                    /*timeL = BuildingTime(Math.Min(GameConstants.MagazineCount - 1, 10), 600, 28800, 10);
                    timeE = 0;*/
                    ConstructionStartTime = DateTime.Now;
                    StartTime = CurrentTime;
                    ConstructionEndTime = DateTime.Now.AddSeconds(BuildingTime(Math.Min(GameConstants.MagazineCount - 1, 10), 600, 28800, 10));

                    GameConstants.YMoney -= 40;
                    GameConstants.Money -= 100;

                    progressBar.transform.localScale -= new Vector3(.2f, .7f);
                    progressBar = Instantiate(gameConstants.contourPrefab,
                        new Vector3(buildingInProgress.transform.position.x - buildingInProgress.transform.localScale.x * .45f,
                        buildingInProgress.transform.position.y + buildingInProgress.transform.localScale.y * .2f,
                        -2)
                        , Quaternion.identity) as GameObject;
                }
                if (counter == false)
                {
                    building = false;
                    floors[y][x] = new BasicRoom(Instantiate((contour ? gameConstants.contourPrefab : gameConstants.YroomPrefab),
                    new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                    + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0)
                    ,
                    gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), Quaternion.identity), x, y, "Yellow Room" + GameConstants.YRoomCount);

                    GameConstants.MagazineCount *= 2;
                    GameConstants.YRoomCount += 1;
                    GameConstants.AllRoomCount += 1;
                    BuildMenuScript.licz = 6;

                    questList.Where(q => q.type == Quest.QuestType.BUILD &&
                                    (q.color == Quest.Color.YELLOW || q.color == Quest.Color.ANY))
                                    .ToList().ForEach(q => q.AddCount());

                    aftcounter = false;
                }
            }
            else if (BuildMenuScript.licz == 4 && mainObject.GetComponent<GameConstants>().click == false)
            {
                if (aftcounter == false)
                {
                    building = true;
                    clone = Instantiate(gameConstants.timeText, new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                   + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0) - .25f
                   ,
                   (gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y) + .34f, -3), Quaternion.identity) as GameObject;

                    buildingInProgress = Instantiate(gameConstants.banner, new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                   + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0),
                   (gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), -1), Quaternion.identity) as GameObject;

                    //  print("counter=true   aftcounter=false");
                    counter = true;
                    // timeL = 120*GameConstants.PurpleRoomC;
                    /*  timeL = BuildingTime(Math.Min(GameConstants.PurpleRoomC - 1, 10), 1800, 86400, 10);
                      timeE = 0;*/
                    ConstructionStartTime = DateTime.Now;
                    StartTime = CurrentTime;
                    ConstructionEndTime = DateTime.Now.AddSeconds(BuildingTime(Math.Min(GameConstants.PurpleRoomC - 1, 10), 1800, 86400, 10));

                    GameConstants.PMoney -= 60;
                    GameConstants.Money -= 200;

                    progressBar = Instantiate(gameConstants.contourPrefab,
                        new Vector3(buildingInProgress.transform.position.x - buildingInProgress.transform.localScale.x * .45f,
                        buildingInProgress.transform.position.y + buildingInProgress.transform.localScale.y * .2f,
                       -2)
                        , Quaternion.identity) as GameObject;
                    progressBar.transform.localScale -= new Vector3(.2f, .7f);
                }
                if (counter == false)
                {
                    building = false;
                    floors[y][x] = new BasicRoom(Instantiate((contour ? gameConstants.contourPrefab : gameConstants.ProomPrefab),
                    new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                    + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0)
                    ,
                    gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), Quaternion.identity), x, y, "Purple Room" + GameConstants.PRoomCount);

                    GameConstants.PRoomCount += 1;
                    GameConstants.PurpleRoomC += 1;
                    GameConstants.AllRoomCount += 1;
                    BuildMenuScript.licz = 6;

                    questList.Where(q => q.type == Quest.QuestType.BUILD &&
                                    (q.color == Quest.Color.PURPLE || q.color == Quest.Color.ANY))
                                    .ToList().ForEach(q => q.AddCount());

                    aftcounter = false;
                }
            }

        }

        else
        {
            // print("Countor: true");
            floors[y][x] = new ContourRoom(Instantiate((contour ? gameConstants.contourPrefab : gameConstants.roomPrefab),
            new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

            + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0)
            ,
            gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), Quaternion.identity), x, y);

        }



    }

    public void AddElevator(int y, bool contour)
    {
        var list = new List<Room>();
        for (int i = 0; i < 5; i++)
        {
            list.Add(null);
        }
        if (contour == false)
        {
            /* if (aftcounter == false)
             {
                 building = true;
                 clone = Instantiate(gameConstants.timeText, new Vector3(gameConstants.BaseRoomPosition.x + (x + .5f) * gameConstants.RoomSize.x

                + (x > 0 ? gameConstants.ElevatorSize.x - gameConstants.RoomSize.x : 0) - .25f
                ,
                (gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y) + .15f, -1), Quaternion.identity) as GameObject;

                 print("counter=true   aftcounter=false");
                 counter = true;
                 timeL = ;
             }*/
            list[1] = new Elevator(Instantiate(gameConstants.elevetorPrefab, new Vector3(gameConstants.BaseRoomPosition.x + gameConstants.RoomSize.x
        + gameConstants.ElevatorSize.x / 2, gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), Quaternion.identity), y);
            //GameConstants.Money -= 15;
        }
        else
        {
            list[1] = new ContourElevator(Instantiate(gameConstants.contourElevetorPrefab,
                new Vector3(gameConstants.BaseRoomPosition.x + gameConstants.RoomSize.x
            + gameConstants.ElevatorSize.x / 2, gameConstants.BaseRoomPosition.y + y * gameConstants.RoomSize.y), Quaternion.identity),
            y
            );
            GameConstants.Money -= 15;
        }

        //  GameConstants.Money -= 15;
        floors[y] = list;
    }

    void Start()
    {
        gameConstants = mainObject.GetComponent<GameConstants>();
        AddElevator(0, false);
        AddRoom(0, 0, false);
        Screen.orientation=ScreenOrientation.LandscapeLeft;

        var questString = new WWW("192.168.43.106:8000 /quest");

        WaitForSeconds w;
        while (!questString.isDone)
            w = new WaitForSeconds(.1f);

        var qj = JsonUtility.FromJson<Quest.QuestJson>(questString.text);

        var q = new Quest(qj);
        if (!questList.Any(x => x.id == q.id)) {
            questList.Add(q);
        }
        print("Color: " + q.color);
        print("reqardColor: " + q.rewardColor);
        print("reward amount: " + q.rewardAmount);
        print("type: " + q.type);
        print("count: " + q.count);

        if (q.type.ToString() == "BUILD")
        {
            GameObject.Find("QuestLog").GetComponent<UnityEngine.UI.Text>().text = "Build "+q.count.ToString()+" "+q.color+" rooms. Reward: "+q.rewardAmount+" "+q.rewardColor+" points" ;
            GameObject.Find("Qstatus").GetComponent<UnityEngine.UI.Text>().text = "Status: In progress!";
            StartCoroutine(CloseQuestLog());
            CloseQuestLog();
        }

        camera = GameObject.Find("Main Camera");
        camera.GetComponent<Camera>().orthographicSize = 20;
    }

    public IEnumerator CloseQuestLog()
    {
        yield return new WaitForSeconds(3f);
        GameObject.Find("QuestCanvas").GetComponent<Canvas>().enabled = false;
    }

    IEnumerable<Vector2> EmptyFields()
    {
        foreach (var f in floors)
        {
            if (f.Value[0] == null && (f.Key <= 0 || floors[f.Key - 1][0] is BasicRoom)) yield return new Vector2(0, f.Key);
            for (int i = 1; i < 5; i++)
            {
                if (f.Value[i] == null && (f.Key <= 0 || floors[f.Key - 1][i] is BasicRoom))
                {
                    yield return new Vector2(i, f.Key);
                    break;
                }
            }
        }
    }

    IEnumerable<Room> AllRooms()
    {
        foreach (var f in floors)
        {
            for (int i = 0; i < 5; i++)
            {
                if (f.Value[i] != null) yield return f.Value[i];
            }
        }
    }

    public Room RoomByGameObject(GameObject go)
    {
        return AllRooms().First(x => x.gameObject.Equals(go));
    }

    public void AddGreen()
    {
        foreach (var v in EmptyFields())
        {
            AddRoom((int)v.x, (int)v.y, true);
        }


    }

    public void AddGreenElevator()
    {
        int? min = null;
        int? max = null;
        foreach (var k in floors.Keys)
        {
            bool b = false;
            for (int i = 0; i < 5; i++)
            {
                if (floors[k][i] != null)
                {
                    b = true;
                }
            }
            if (b == true)
            {
                if (min == null || min > k) min = k;
                if (max == null || max < k) max = k;
            }
        }
        min -= 1;
        AddElevator(min.GetValueOrDefault(0), true);
        if (floors[max.GetValueOrDefault(0)].Where(x => x != null).Count() > 1)
        {
            max += 1;
            AddElevator(max.GetValueOrDefault(0), true);
        }
    }

    public void ClearGreen()
    {
        foreach (var f in floors)
        {
            for (int i = 0; i < 5; i++)
            {
                if (f.Value[i] != null && (f.Value[i] is ContourRoom || f.Value[i] is ContourElevator))
                {
                    Destroy(f.Value[i].gameObject);
                    f.Value[i] = null;
                }
            }
        }
    }

    public void TimeCounter(float timeLeft)
    {
        timeLeft -= Time.deltaTime;
        clone.GetComponent<TextMesh>().text = "Left: " + timeLeft;

        if (timeLeft < 0)
            counter = false;
    }

    public IEnumerator WwW()
    {
        
        www = new WWW("http://localhost:8000");
        yield return new WaitForSeconds(1f);
        CurrentTime = int.Parse(www.text);
       // Debug.Log("Time: " + CurrentTime);
        rob = false;
    }

    public IEnumerator DroneCount()
    {
        print("Drone Bought!");
        yield return new WaitForSeconds(1f);
        print("MINUS!");
        EndTime = EndTime - 2;
        GameConstants.buyDrone = true;
    }

    float timeLeft = 60.0f;
    void Update()
    {
        /* if (Input.GetKeyDown(KeyCode.W))
         {
             foreach(var i in floors)
             {
                 print("a " + i.ToString());
             }
         }*/
        // 
        //
        // OBLICZANIE HAJSU
        if (camera.GetComponent<Camera>().orthographicSize >= 7)
        {
            camera.GetComponent<Camera>().orthographicSize-=.1f;
        }
        if (rob == false)
        {
            
            StartCoroutine(WwW());
            rob = true;
            
        }

        if (GameConstants.questFlag == true)
        {
            GameConstants.questFlag = false;
            Qrob++;
            var questString = new WWW("192.168.43.106:8000/quest"+Qrob);

            WaitForSeconds w;
            while (!questString.isDone)
                w = new WaitForSeconds(.1f);

            var qj = JsonUtility.FromJson<Quest.QuestJson>(questString.text);

            var q = new Quest(qj);
            if (!questList.Any(x => x.id == q.id))
            {
                questList.Add(q);
            }
    /*        print("Color: " + q.color);
            print("reqardColor: " + q.rewardColor);
            print("reward amount: " + q.rewardAmount);
            print("type: " + q.type);
            print("count: " + q.count);*/

            if (q.type.ToString() == "BUILD")
            {
                GameObject.Find("QuestLog").GetComponent<UnityEngine.UI.Text>().text = "Build " + q.count.ToString() + " " + q.color + " rooms. Reward: " + q.rewardAmount + " " + q.rewardColor + " points";
                GameObject.Find("Qstatus").GetComponent<UnityEngine.UI.Text>().text = "Status: In progress!";
                StartCoroutine(CloseQuestLog());
                CloseQuestLog();
            }
            if (q.type.ToString() == "GATHER")
            {
                GameObject.Find("QuestLog").GetComponent<UnityEngine.UI.Text>().text = "Gather " + q.count.ToString() + " " + q.color + " points. Reward: " + q.rewardAmount + " " + q.rewardColor + " points";
                GameObject.Find("Qstatus").GetComponent<UnityEngine.UI.Text>().text = "Status: In progress!";
                StartCoroutine(CloseQuestLog());
                CloseQuestLog();
            }
            if (Qrob >= 5)
                Qrob = 1;

        }
        
        if ((DateTime.Now - LastMoneyUpdate).TotalSeconds > 1)
        {
            if (GameConstants.Money <= (100 * GameConstants.MagazineCount))
                GameConstants.Money += GameConstants.RoomCount;
            if (GameConstants.RMoney <= (100 * GameConstants.MagazineCount))
                GameConstants.RMoney += GameConstants.RRoomCount;
            if (GameConstants.YMoney <= (100 * GameConstants.MagazineCount))
                GameConstants.YMoney += GameConstants.YRoomCount;
            if (GameConstants.PMoney <= (100 * GameConstants.MagazineCount))
                GameConstants.PMoney += GameConstants.PRoomCount;
            GameConstants.UpdatePoints();
            questList.Where(x => x.type == Quest.QuestType.GATHER).ToList().ForEach(x => x.Check());
            LastMoneyUpdate = DateTime.Now;
        }

        if (counter == true)
        {
            timeL = (float)(EndTime - CurrentTime);
            timeE = (float)(StartTime + CorTime);
            float percent = timeE / (timeE + timeL);
            if (GameConstants.buyDrone2 == true)
            {
                if (GameConstants.buyDrone == true)
                {
                    GameConstants.buyDrone = false;
                    StartCoroutine(DroneCount());
                    DroneCount();
                }
            }
            else if (GameConstants.buyDrone2 == false && GameConstants.buyDrone == false)
            {
                try
                {
                    Destroy(GameObject.Find("Drone(Clone"));
                    Destroy(GameObject.Find("DroneLaser(Clone"));
                }
                catch
                {
                    //print("faildorne");
                }
            }
            clone.GetComponent<TextMesh>().text = "Left: " + Math.Round(timeL);
            if (progressBar != null)
            {
                progressBar.transform.localScale = new Vector3(percent * .8f * buildingInProgress.transform.localScale.x, .2f, 1);

                progressBar.transform.position =
                    new Vector3(buildingInProgress.transform.position.x - buildingInProgress.transform.localScale.x / 2
                    + buildingInProgress.transform.localScale.x / 10
                    + progressBar.transform.localScale.x / 2,
                    progressBar.transform.position.y, progressBar.transform.position.z);

                // print("Time: " + timeL);
            }
            if (CurrentTime >= EndTime)
            {
                //  print("X " + X);
                //  print("Y " + Y);
                Destroy(clone);
                Destroy(buildingInProgress);
                try
                {
                    Destroy(GameObject.Find("Drone(Clone)"));
                    Destroy(GameObject.Find("DroneLaser(Clone)"));
                }
                catch { }
                aftcounter = true;
                counter = false;
                // print("End: " + timeL);
                AddRoom(X, Y, false);

                if (progressBar != null)
                    GameObject.Destroy(progressBar);
                //StartTime = 0;
               // EndTime = 0;
               // CorTime = 0;

            }
        }
        if (warningCounter == true)
        {
            warnTime -= Time.deltaTime;
            if (warnTime <= 0)
            {
                GameObject.Find("WarningText").GetComponent<UnityEngine.UI.Text>().text = " ";
                warningCounter = false;
            }

        }

    }
}