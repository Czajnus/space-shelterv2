﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsScript : MonoBehaviour {

    private GameObject purchaseMenu;
    private IAPManager cos;

    // Use this for initialization
    void Start () {
        cos = new IAPManager();
        purchaseMenu = GameObject.Find("PurchaseMenu");
    }

    public void Buy50Points()
    {
        //IAPManager.Instance.Buy50Points();
        cos.Buy50Points();

    }
    public void Buy100Points()
    {
        //  IAPManager.Instance.Buy50Points();
        cos.Buy100Points();
    }
    public void Buy200Points()
    {
        // IAPManager.Instance.Buy50Points();
        cos.Buy200Points();
    }

    public void CLose()
    {
        purchaseMenu.GetComponent<Canvas>().enabled = false;
    }


}
