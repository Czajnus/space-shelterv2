﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room {

    public GameObject gameObject;
    public int x;
    public int y;

    public Room(GameObject go) {
        gameObject = go;
    }
    public Room(GameObject go, int x, int y) {
        gameObject = go;
        this.x = x;
        this.y = y;
    }

}
