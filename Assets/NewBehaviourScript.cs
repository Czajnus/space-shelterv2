﻿using UnityEngine;
using Assets;

public class NewBehaviourScript : MonoBehaviour {

    public GameObject buildMenu;
    private GameObject mainObject;
    // Use this for initialization
    void Start() {
        buildMenu = GameObject.Find("BuildMenu");
        mainObject = GameObject.Find("mainObject");

    }

    // Update is called once per frame
    void Update() {

    }

    public void OnMouseDown() {
        if (GameObject.Find("mainObject").GetComponent<GameConstants>().click == false)
        {
            GameObject.Find("mainObject").GetComponent<GameConstants>().click = true;
            buildMenu.GetComponent<Canvas>().enabled = true;
            //   GameObject.Find("mainObject").GetComponent<Main>().AddGreen();
        }
    }

    public void COINS(){

        GameConstants.Money += 1000;
        GameConstants.RMoney += 1000;
        GameConstants.YMoney += 1000;
        GameConstants.PMoney += 1000;

        }

    public void Build() {

    }

}
