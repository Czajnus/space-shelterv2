﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameConstants : MonoBehaviour {
    public GameObject roomPrefab;
    public GameObject WroomPrefab;
    public GameObject RroomPrefab;
    public GameObject YroomPrefab;
    public GameObject ProomPrefab;
    public GameObject contourPrefab;
    public GameObject elevetorPrefab;
    public GameObject contourElevetorPrefab;
    public GameObject timeText;
    public GameObject banner;
    public GameObject drone;
    public GameObject droneLaser;

    public Sprite WroomLv1;
    public Sprite WroomLv2;
    public Sprite WroomLv3;
    public Sprite WroomLv4;

    public Sprite RroomLv1;
    public Sprite RroomLv2;
    public Sprite RroomLv3;
    public Sprite RroomLv4;

    public Sprite YroomLv1;
    public Sprite YroomLv2;
    public Sprite YroomLv3;
    public Sprite YroomLv4;

    public Sprite ProomLv1;
    public Sprite ProomLv2;
    public Sprite ProomLv3;
    public Sprite ProomLv4;


    public string RoomName;
    public int RoomKind;

    public bool upgrading = false;

    public Vector2 RoomSize = new Vector2(3f, 1.1f);
    public Vector2 BaseRoomPosition = new Vector2(-1, -1.5f);

    public Vector2 ElevatorSize = new Vector2(.85f, 1.1f);

    public static int RoomCount = 0;
    public static int Money = 0;
    public static float RRoomCount = 0;         //ZMIENNE ODPOWIADAJACE ZA HAJS I LICZBE POKOI
    public static float RMoney = 0;             // PIERWSZE LITERY TO LITERY KOLORW R-RED, Y-YELLOW ITP.
    public static float YRoomCount = 0;
    public static float YMoney = 0;
    public static float PRoomCount = 0;
    public static float PMoney = 0;

    public static int buildLimit = 3;

    public static int MagazineCount=1;
    public static int WhiteRoomC = 1;
    public static int RedRoomC = 1;
    public static int PurpleRoomC = 1;

    public static int AllRoomCount = 0;

    public static float roomX = 0;
    public static float roomY = 0;

    public static int Mo = 0;// Math.Round(Money);
    public static int Ro = 0;
    public static int Yo=0;
    public static int Po=0;

    public static int mainCost = 100;
    public static int whiteCost1 = 100;
    public static int redCost1 = 100;
    public static int redCost2 = 50;
    public static int yellowCost1 = 200;
    public static int yellowCost2 = 150;
    public static int purpleCost1 = 300;
    public static int purpleCost2 = 250;

    public static bool buyDrone = false;
    public static bool buyDrone2 = false;

    public static bool questFlag = false;



    public bool click = false;

    public static void UpdatePoints()
    {
        Mo = Money;
        Ro = (int)RMoney;
        Yo = (int)YMoney;
        Po = (int)PMoney;

        //print("rr: " + RoomCount);
        
            GameObject.Find("Points").GetComponent<Text>().text = Mo.ToString() + " (+" + RoomCount.ToString() + "/s)";
            GameObject.Find("Red Points").GetComponent<Text>().text = Ro.ToString() + " (+" + RRoomCount.ToString() + "/s)";
            GameObject.Find("Yellow Points").GetComponent<Text>().text = Yo.ToString() + " (+" + YRoomCount.ToString() + "/s)";
            GameObject.Find("Purple Points").GetComponent<Text>().text = Po.ToString() + " (+" + PRoomCount.ToString() + "/s)";
        GameObject.Find("Magazine").GetComponent<Text>().text = (100 * MagazineCount).ToString();
        GameObject.Find("Limit").GetComponent<Text>().text = AllRoomCount.ToString() +"/"+ buildLimit.ToString();
    }

    /*
    public GameObject buildRoomPrefab;

    public Canvas uiCanvas;

    public Vector2 RoomSize = new Vector2(3f, 1.1f);
    public  Vector2 BaseRoomPosition = new Vector2(-1f, -1.5f);

    public static Vector2 maximumBuildingSize = new Vector2(3, 5);

    public static GameObject[,] BuildingGrid = new GameObject[(int)(maximumBuildingSize.y), (int)(maximumBuildingSize.x)];
    public static GameObject[,] GreenGrid = new GameObject[(int)(maximumBuildingSize.y), (int)(maximumBuildingSize.x)];

    public static int RoomCount = 0;

    public static int Money = 0;

    public static void AddRoom(int x, int y, GameObject r, bool b) {
        if (b == true) {
            BuildingGrid[y, x] = r;
            RoomCount++;
            UpdatePoints();
        }
        else
            GreenGrid[y, x] = r;
    }

    public static void UpdatePoints() {
        GameObject.Find("Points").GetComponent<Text>().text = Money.ToString() + " (+" + RoomCount.ToString() + "/s)";
    }

    public static void ClearGrid() {
        for (int i = 0; i < maximumBuildingSize.y; i++) {
            for (int j = 0; j < maximumBuildingSize.x; j++) {
                if (GreenGrid[i, j] != null) {
                    Destroy(GreenGrid[i, j]);
                    GreenGrid[i, j] = null;
                }
            }
        }
    }

    public static IEnumerable<Vector2> PossibleBuildFields() {
        for (int i = 0; i < maximumBuildingSize.y; i++) {
            for (int j = 0; j < maximumBuildingSize.x; j++) {
                if (BuildingGrid[i, j] == null && (i == 0 || BuildingGrid[i - 1, j] != null)
                 && (j == 0 || BuildingGrid[i, j - 1] != null)) {
                    yield return new Vector2(j, i);
                }
            }
        }
    }
    */
}

